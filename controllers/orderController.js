const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');

const orderProduct = async (req, res) => {
    try {

        const verifiedUser = await auth.decode(req.headers.authorization);

        if (verifiedUser.isAdmin) {
            return res.send('You are an Admin');
        }

        const product = req.body.products

        if (product.length == 0) {
            return res.send("Product order is required");
        }


        //create new object with subtotal
        const arr = product.map((obj) => {
            let subtotal = obj.quantity * obj.price
            return {
                productId: obj.productId,
                price: obj.price,
                quantity: obj.quantity,
                subtotal: subtotal
            }
        });

        //get total amount
        const total = arr.map((obj) => obj.subtotal).reduce((val, currentVal) => val + currentVal);


        //order schema
        const newOrder = new Order({
            userId: verifiedUser.id,
            products: arr,
            totalAmount: total
        })

        const save = await newOrder.save();

        //update product count, productCount - order quantity
        for (let i = 0; i < arr.length; i++) {
            let id = arr[i].productId;
            let quantity = arr[i].quantity;
            await Product.findByIdAndUpdate(id, { $inc: { productCount: -quantity } });
        }

        return res.send(save);


    } catch (error) {
        return res.send(error.message);
    }

};


//ordered products
const orderedProduct = async (req, res) => {
    try {

        const verifiedUser = await auth.decode(req.headers.authorization);

        if (verifiedUser.isAdmin) {
            const getUsersAllOrder = await Order.find({}, { __v: 0 });
            return res.send(getUsersAllOrder);
        }

        const getAllOrder = await Order.find({ userId: { $eq: verifiedUser.id } }, { userId: 0, __v: 0 });
        return res.send(getAllOrder)

    } catch (error) {
        return res.send(error.message);
    }
};


module.exports = {
    orderProduct,
    orderedProduct
}